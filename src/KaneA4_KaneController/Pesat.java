package KaneA4_KaneController;

import java.util.concurrent.Callable;

public class Pesat implements Callable {

	String nombre;

	private boolean saludado = false;
	private Kane m_kane;

	public Pesat(Kane k, String n) {
		m_kane = k;
		nombre = n;
	}
	

	@Override
	public Object call() {
		// TODO Auto-generated method stub

		try {

			while (true) {

				synchronized (m_kane.llegar) {
					m_kane.llegar.wait();
				}


				if (!m_kane.DarSaludo()) { // su lo que voy a saludar no existe
					System.out.println(Thread.currentThread().getName() + " | " + nombre + "	: no a podido saludar AlKane");
					// return false;
				} 
				else {

					Thread.sleep(500);

					synchronized (m_kane.saludo) {
						m_kane.saludo.notify();
						
						System.out.println(Thread.currentThread().getName() + " | Hola Kane que tal, soy " + nombre );
						
						m_kane.registrarSaludo(this);
						
					}
					
					
				}
			}
		} catch (InterruptedException e) {
			System.out.println("EL KANE NO ME QUIERE :Z");
		}
		return true;

		

	}
}
