package KaneA4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

// kane a4

public class Main {

	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();

		ArrayList<Future<Boolean>> futuros = new ArrayList<Future<Boolean>>();

		Kane kane = new Kane();

		executor.submit(new Pesat(kane, "Pau"));
		executor.submit(new Pesat(kane, "Eric"));
		executor.submit(new Pesat(kane, "Gerar"));
		executor.submit(new Pesat(kane, "Arnau"));
		executor.submit(new Pesat(kane, "Marta"));
//		executor.submit(new Pesat(kane, "Valenti"));
//		executor.submit(new Pesat(kane, "Alber"));
//		executor.submit(new Pesat(kane, "Juli"));
//		executor.submit(new Pesat(kane, "Luis"));
//		executor.submit(new Pesat(kane, "Leiva"));
//		executor.submit(new Pesat(kane, "Alex"));
//		executor.submit(new Pesat(kane, "Carmen"));
//		executor.submit(new Pesat(kane, "Iker"));
//		executor.submit(new Pesat(kane, "Sergio"));
//		executor.submit(new Pesat(kane, "Ismael"));
//		executor.submit(new Pesat(kane, "Willy"));
//		executor.submit(new Pesat(kane, "Walter"));
//		executor.submit(new Pesat(kane, "Xavi"));
//		executor.submit(new Pesat(kane, "Raul"));
//		executor.submit(new Pesat(kane, "Cesc"));

		
		futuros.add(executor.submit(kane));
		//executor.submit(kane);
		executor.shutdown(); // dejo de aceptar gente

	
		for (Future<Boolean> futuro : futuros) {
			try {
				futuro.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		// cuando todos acaban
		
		executor.shutdownNow();
		try {
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		

	}

}
