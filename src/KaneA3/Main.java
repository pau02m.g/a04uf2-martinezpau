package KaneA3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

// kane a3

public class Main {

	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();

		ArrayList<Future<Boolean>> futuros = new ArrayList<Future<Boolean>>();

		Kane kane = new Kane();

		futuros.add(executor.submit(new Pesat(kane, "Pau")));
		futuros.add(executor.submit(new Pesat(kane, "Eric")));
		futuros.add(executor.submit(new Pesat(kane, "Gerar")));
		futuros.add(executor.submit(new Pesat(kane, "Arnau")));
		futuros.add(executor.submit(new Pesat(kane, "Marta")));
		futuros.add(executor.submit(new Pesat(kane, "Valentina")));
		futuros.add(executor.submit(new Pesat(kane, "Alber")));
		futuros.add(executor.submit(new Pesat(kane, "Juli")));
		futuros.add(executor.submit(new Pesat(kane, "Luis")));
		futuros.add(executor.submit(new Pesat(kane, "Leiva")));
		futuros.add(executor.submit(new Pesat(kane, "Alex")));
		futuros.add(executor.submit(new Pesat(kane, "Carmen")));
		futuros.add(executor.submit(new Pesat(kane, "Iker")));
		futuros.add(executor.submit(new Pesat(kane, "Sergio")));
		futuros.add(executor.submit(new Pesat(kane, "Ismael")));
		futuros.add(executor.submit(new Pesat(kane, "Willy")));
		futuros.add(executor.submit(new Pesat(kane, "Walter")));
		futuros.add(executor.submit(new Pesat(kane, "Xavi")));
		futuros.add(executor.submit(new Pesat(kane, "Raul")));
		futuros.add(executor.submit(new Pesat(kane, "Cesc")));

		executor.submit(kane);
		executor.shutdown(); // dejo de aceptar gente

	
		for (Future<Boolean> futuro : futuros) {
			try {
				futuro.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		// cuando todos acaban
		
		executor.shutdownNow();
		try {
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		

	}

}
