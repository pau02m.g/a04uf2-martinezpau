package KaneA2;

import java.util.ArrayList;
import java.util.concurrent.Callable;


public class Kane implements Callable<Boolean> {

	public ArrayList<Object> Saludos = new ArrayList<Object>();

	Object llegar = new Object();
	
	Object saludo = new Object();

	public synchronized boolean DarSaludo() {
		if (Saludos.size() > 0) {
			Saludos.remove(0);
			return true;
		}
		return false;
	}

	@Override
	public Boolean call() {
		try {

			Thread.sleep(100);

			//for (int i = 0; i < 5; i++)
			Saludos.add(new Object());

			synchronized (llegar) {
				llegar.notifyAll();
			}

			while (true) {
				synchronized (saludo) {
					saludo.wait();
					//Thread.sleep(100);
					synchronized (llegar) {
						
						Saludos.add(new Object());
						llegar.notifyAll();
					}
				}
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("YA ME HAN SALUDADO TODOS");
		}

		return true;
	}

}
