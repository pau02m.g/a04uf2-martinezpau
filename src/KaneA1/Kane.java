package KaneA1;

import java.util.ArrayList;
import java.util.concurrent.Callable;


public class Kane implements Callable<Boolean>{

	
	ArrayList<Object> Saludos = new ArrayList<Object>();
	
	Object saludo;

	public synchronized Object DarSaludo()
	{
		if(Saludos.size() > 0)
		{
			saludo = Saludos.remove(0);
			return saludo;
		}
		return null;
	}

	
	
	@Override
	public Boolean call(){
		try {
		
			Thread.sleep(100);
			
			for(int i = 0; i < 1; i++)
				Saludos.add(new Object());

			
			synchronized(this) {
				this.notifyAll();
			}
			
			
			synchronized(saludo) {
				saludo.wait();
			}
			
			
			
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}




}
