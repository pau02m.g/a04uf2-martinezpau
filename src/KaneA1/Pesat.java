package KaneA1;

import java.util.concurrent.Callable;

public class Pesat implements Callable{

	String nombre;
	
	
	private Kane m_kane;
	
	
	
	public Pesat(Kane k, String n) {
		m_kane = k;
		nombre = n;
	}

	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		
		try
		{

		
		
			synchronized(m_kane) {
				m_kane.wait();
			}
			
			//contolador
			Object VoyaSaludar = m_kane.DarSaludo();
			
			
			if(VoyaSaludar == null) { // su lo que voy a saludar no existe
				System.out.println(Thread.currentThread().getName() + " | " + nombre + ": no a podido saludar AlKane");
				return false;
			}
			
		
			Thread.sleep(100);
			System.out.println(Thread.currentThread().getName() + " | Hola Kane que tal, soy " + nombre);
			
			synchronized(m_kane.saludo) {
				m_kane.saludo.notify();
			}
			
			
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
		return true;

	}
}
